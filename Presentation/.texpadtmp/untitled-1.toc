\beamer@sectionintoc {1}{Introduction}{2}{0}{1}
\beamer@subsectionintoc {1}{1}{Project Description}{2}{0}{1}
\beamer@subsectionintoc {1}{2}{App. Description}{4}{0}{1}
\beamer@sectionintoc {2}{The Data Sets}{5}{0}{2}
\beamer@subsectionintoc {2}{1}{Users Data}{5}{0}{2}
\beamer@sectionintoc {3}{Cleaning The Data Set}{9}{0}{3}
\beamer@sectionintoc {4}{Exploratory Data Analysis}{12}{0}{4}
\beamer@sectionintoc {5}{User Data EDA}{13}{0}{5}
\beamer@sectionintoc {6}{Per User Workout data EDA}{18}{0}{6}
\beamer@sectionintoc {7}{Per Cluster Workout data EDA}{20}{0}{7}
\beamer@sectionintoc {8}{Workout amount clustering}{23}{0}{8}
\beamer@subsectionintoc {8}{1}{10 to 30 Workouts}{24}{0}{8}
\beamer@subsectionintoc {8}{2}{31 to 50 Workouts}{26}{0}{8}
\beamer@subsectionintoc {8}{3}{51 to 100 Workouts}{28}{0}{8}
\beamer@subsectionintoc {8}{4}{100+ Workouts}{30}{0}{8}
\beamer@sectionintoc {9}{Conclusions}{32}{0}{9}
\beamer@sectionintoc {10}{Future Work}{34}{0}{10}
